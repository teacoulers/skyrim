#include "Hra.h"



    Hra::Hra(string jmenoDovakina)
    {
        m_dovakin = new Dovakin(jmenoDovakina, 100);
    }

    void Hra::vytvorNovouHru()
    {
        string jmeno = "";
        cout << "Zadejte jmeno dovakina: ";
        cin >> jmeno;
        cout << endl;

        s_hraSingleton = new Hra(jmeno);
        s_hraSingleton->vytvorMesta();
    }

    void Hra::vytvorMesta()
    {
        m_mesta.push_back(new Mesto("Whiterun"));
        m_mesta.push_back(new Mesto("Windhelm"));
        m_mesta.push_back(new Mesto("Riften"));
        m_mesta.push_back(new Mesto("Solitude"));
        m_mesta.push_back(new Mesto("Markarth"));
        m_mesta.push_back(new Mesto("Falkreath"));
        m_mesta.push_back(new Mesto("Morthal"));
    }

    Hra* Hra::getHra(){
        if (s_hraSingleton == NULL) {
            vytvorNovouHru();
        }
        return s_hraSingleton;
    }

    Dovakin* Hra::getDovakin(){
        return m_dovakin;
    }

    Mesto* Hra::getMestoPodleIndexu(int index){
        return m_mesta.at(index);
    }

    int Hra::getPocetMest(){
        return m_mesta.size();
    }

    void Hra::vypsatMesta(){
        for(unsigned int i = 0; i < m_mesta.size(); i++){
            cout << m_mesta.at(i)->getNazevMesta() << "[" << i+1 << "], ";
        }
    }

Hra* Hra::s_hraSingleton = NULL;
