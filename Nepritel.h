#ifndef Nepritel_H
#define Nepritel_H
#include <iostream>

using namespace std;

class Nepritel {

private:
	std::string m_jmeno;
	int m_zivot;

public:
	Nepritel(std::string jmeno, int zivot);

	std::string getJmeno();

	int getZivot();

	void printInfo();
};

 #endif
