#include "Nepritel.h"

Nepritel::Nepritel(string jmeno, int zivot)
{
    m_jmeno = jmeno;
    m_zivot = zivot;
}

string Nepritel::getJmeno()
{
      return m_jmeno;
}

int Nepritel::getZivot()
{
    return m_zivot;
}


void Nepritel::printInfo()
{
    cout << "Nepritel: " << m_jmeno << " ,zivot: " << m_zivot << endl;
}
