#ifndef Hra_H
#define Hra_H


#include "Dovakin.h"
#include <vector>
#include <iostream>
#include "Mesto.h"

using namespace std;

class Hra
{


private:
    static Hra* s_hraSingleton;
    Dovakin* m_dovakin;
    std::vector<Mesto*> m_mesta;

    Hra(string jmenoDovakina);

    static void vytvorNovouHru();

    void vytvorMesta();


public:

static Hra* getHra();

  Dovakin* getDovakin();


    Mesto* getMestoPodleIndexu(int index);


    int getPocetMest();


    void vypsatMesta();
};

#endif
