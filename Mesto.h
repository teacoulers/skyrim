#ifndef Mesto_H
#define Mesto_H


#include "Nepritel.h"
#include <iostream>

using namespace std;

class Mesto {

 private:
    std::string m_nazev;
    Nepritel* m_nepritel;

public:
	Mesto(std::string nazevMesta);

	std::string getNazevMesta();

	Nepritel* getNepritel();
};

#endif
