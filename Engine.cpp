#include "Engine.h"
#include "Hra.h"


#include<iostream>

using namespace std;



    int Engine::nactiCislo(int pocetMoznosti, std::string popis){
        int nacteno = 0;
        do {
            cout << popis << endl;
            cin >> nacteno;
        } while (nacteno < 0 || nacteno > pocetMoznosti-1);
        return nacteno;
    }

    int Engine::startMenu(){
            int nacteno = nactiCislo(3, "Zadejte cislo: 1 - Nova hra, 2 - Nacist hru, 0 - Konec");
            switch (nacteno){
            case 0:
                cout << "Hra ukoncena." << endl;
                break;
            case 1:
                Hra::getHra();
                break;
            case 2:
                cout << "Nekdy v budoucnu." << endl;
                break;
            default:
                cout << "Chybny vstup." << endl;
        }
        return nacteno;
    }

      void Engine::spustitHru(){
        Engine* engine = new Engine();

        if(engine-> startMenu() != 0) {
            cout << "Hra spustena" << endl;
            while(engine-> herniMenu() != 0){};
        }


    }

    int Engine::herniMenu(){

        Hra* hra = Hra::getHra();
        cout << endl << "Hrac se nechazi ve meste: " << hra->getMestoPodleIndexu( hra->getDovakin()->getPozice() )->getNazevMesta() << endl;
        int noveMesto = 0;
        Nepritel* nepritel = NULL;
        int nacteno = 0;
        nacteno = nactiCislo(5, "Zadejte cislo: 1 - Prohledat mesto, 2 - Jit do jineho mesta, 3 - Vypsat mesta, 4 - Vypsat informace, 0 - Ukoncit hru");
        switch (nacteno){
        case 0:
            break;
        case 1:
            nepritel = hra->getMestoPodleIndexu( hra->getDovakin()->getPozice() )->getNepritel();
            if (nepritel != NULL){
                cout << "Nalezen nepritel!" << endl;
                nepritel->printInfo();
            } else {
                cout << "Nic nenalezeno" << endl;
            }
            break;
        case 2:
            noveMesto = nactiCislo( hra->getPocetMest() + 1, "Zadejte cislo mesta (vice info v moznosti Vypsat mesta): 0 - Navrat zpet");
            if (noveMesto != 0){
                noveMesto -= 1;
                cout << "Hrac vesel do noveho mesta: " << hra->getMestoPodleIndexu(noveMesto)->getNazevMesta() << endl;
                hra->getDovakin()->setPozice(noveMesto);
            }
            break;
        case 3:
            hra->vypsatMesta();
            cout << endl;
            break;
        case 4:
            hra->getDovakin()->printInfo();
            break;
        }
        return nacteno;
    }

	void Engine::spustitHru();
