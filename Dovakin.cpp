#include "Dovakin.h"

#include<iostream>
using namespace std;

Dovakin::Dovakin(std::string jmeno, int zivot)
{
    m_jmeno = jmeno;
    m_zivot = zivot;
    m_pozice = 0;
}

std::string Dovakin::getJmeno()
{
    return m_jmeno;
}

int Dovakin::getZivot()
{
    return m_zivot;
}

int Dovakin::getPozice()
{
    return m_pozice;
}

void Dovakin::setPozice(int pozice)
{
    m_pozice = pozice;
}

void Dovakin::printInfo()
{
    cout << "Dovakin: Jmeno: " << m_jmeno << ", zivot: "  << m_zivot << endl;
}
