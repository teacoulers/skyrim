#ifndef Engine_H
#define Engine_H

#include <string>
#include<iostream>
#include "Hra.h"

using namespace std;

class Engine
{
private:
    int nactiCislo(int pocetMoznosti, string popis);
    int startMenu();

    int herniMenu();


public:
    static void spustitHru();

};


#endif
